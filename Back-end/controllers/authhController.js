const User = require("../models/User")
const jwt = require("jsonwebtoken")  // imporort tokken
const bcrypt = require('bcryptjs')
exports.register = async (req,res,next) => {
    try {
        //req.body - name , emial , password
        const user = await User.create(req.body);
        const token = jwt.sign({userID: user._id},process.env.APP_SECRET); // tạo tokken key
        res.status(200).json({
            status:"success",
            data: {token, userName: user.name}
        })
    } catch (error) {
        res.json(error)
    }
}

exports.login =async (req,res,next) =>{
   try {
       const user =  await User.findOne({email:req.body.email});
       if(!user){
        // Error L Email is not correct
       }
       if(bcrypt.compareSync(req.body.password, user.password)){
           const token = jwt.sign({userID: user._id},process.env.APP_SECRET);
           res.status(200).json({
            status:"success",
            data: {token, userName: user.name}
        })
       }else{
           // Error password is not correct
       }
   } catch (error) {
     res.json(error)
   }
}

// get current user
exports.getCurrentUser =async (req,res,next) =>{
    try {
       const data = { user: null }
       if(req.user){
        const user = await User.findOne(({ _id: req.user.userId}));
        data.user = { userName: user.name }
       }
       res.status(200).json({
           status: "success",
           data:data
       })
    } catch (error) {
      res.json(error)
    }
 }