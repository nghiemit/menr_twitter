const Post = require("../models/Post");

// Get ALL Posts
exports.getAllPosts = async (req, res, next) => {
  try {
    const posts = await Post.find({}).populate("author","name").select('content createAr');
    res.status(200).json({
      stauts: "success",
      results: posts.length,
      data: {posts},
    });
  } catch (error) {
    res.json(error);
  }
};

//  Create One Post
exports.createOnePost = async (req, res, next) => {
  try {
    const { userId } = req.user;
    const post =await Post.create({ ...req.body, author: userId });
    res.status(200).json({
      stauts: "success",
      results:{post},
    });
  } catch (error) {
    res.json(error);
  }
};


//  UPdate One Post
exports.updateOnePost = async (req, res, next) => {
    try {
      const {postId} = req.params;
      const post = await Post.findByIdAndUpdate(postId,{...req.body},{new:true, runValidator:true})
      res.status(200).json({
        stauts: "success",
        results:{post},
      });
    } catch (error) {
      res.json(error);
    }
  };


  //  UPdate One Post
exports.deletePost = async (req, res, next) => {
    try {
      const {postId} = req.params;
      await Post.findByIdandDelete(postId)
      res.status(200).json({
        stauts: "success",
        results:{post},
      });
    } catch (error) {
      res.json(error);
    }
  };