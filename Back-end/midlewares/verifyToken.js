const jwt = require('jsonwebtoken');

exports.verifyToken = (req,res,next) =>{
    // lấy truyền truy cập gắn ở header
    const Authorization = req.header('authorization');
    if(!Authorization){
        // Error : UnAuthorized
    }

    // get Token
    const token = Authorization.replace('Bearer ','')

    // verify token

    const {userId} = jwt.verify(token, process.env.APP_SECRET)

    // gán nó vào req
    req.user = {userId}
    next();
}