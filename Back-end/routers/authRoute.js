const express = require('express');
const {login,register,getCurrentUser} = require('../controllers/authhController');
const {checkCurrentUser} = require('../midlewares/checkCurrentUser')
const Router = express.Router();

Router.route('/register').post(register);
Router.route('/login').post(login);
Router.route('/').get(checkCurrentUser,getCurrentUser);


module.exports = Router;