const express = require('express');

const {getAllPosts,createOnePost,updateOnePost, deletePost} = require('../controllers/postController');

const {verifyToken} = require("../midlewares/verifyToken");

const Router = express.Router();

Router.route('/').get(getAllPosts).post(verifyToken, createOnePost);

Router.route('/:postId').put(verifyToken, updateOnePost).delete(verifyToken, deletePost);

module.exports = Router;
