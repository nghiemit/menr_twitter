//dotenv
require('dotenv').config();

//connect DB
const connectDB = require('./configs/db')
connectDB(); // gọi

const express = require('express');
const cors = require('cors');

//  import Router 
const authRoute = require('./routers/authRoute')
const postRouter = require('./routers/postRouter')



const app = express();

//cors
app.use(cors())

//body Parser
app.use(express.json());

// mount the route 
app.use('/api/v1/auth', authRoute)
app.use('/api/v1/posts', postRouter)



const port = process.env.APP_PORT;

app.listen(port, () => {
    console.log(`server is running on port ${port}`)
})