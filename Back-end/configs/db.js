const mongoose = require('mongoose');
const connectDB = () => {
    mongoose.connect(process.env.DB_URI, {
        keepAlive: 1,
        useUnifiedTopology: true,
    }).then(() => {
        console.log("DB contection SuccessFull")
    })
        .catch((err) => {
            console.log("Something wrong when connect to DB. " + err);
        })
}

module.exports = connectDB;

// const mongoose = require('mongoose');
// const connectDB = async () => {
//    try {
//     const conn = await mongoose.connect(process.env.DB_URI, {
//         useUnifiedTopoLogy: true,
//         useNewUrlParser: true,
//         useCreateIndex: true
//     })
//     console.log("DB contection SuccessFull")
//    } catch (error) {
//     console.log("Something wrong when connect to DB. " + error);
//     process.exit(1)
//    }
// }
//  module.exports = {connectDB};
