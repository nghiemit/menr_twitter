
// Schema : là 1 cái bộ khung quy định cái cấu trúc của tài nguyên khi chúng ta lưu vào database
// Schame valitation: quy định cơ chế . cấu trúc dữ liệu như (String , unique,trim.....)

const mongoose = require('mongoose');
const bcrypt = require('bcryptjs')

const userSchema =  new mongoose.Schema({
    name : {
        type: String, unique: true, trim: true, required:[true, "Name must be required"]
    },
    email:{
        type: String, unique: true, trim: true, required:[true, "email must be required"]
    },
    password: {
        type: String, trim: true, required:[true, "password must be required"], minlenght:[6,"Password must be at least 6 characters"]
    }
},{timestamps: true})


// mã hóa password
userSchema.pre('save', function(next){
    let user = this;
    bcrypt.hash(user.password,10,function(error, hash){
        if(error){
            return next(error)
        }else{
            user.password = hash;
            next()
        }
    })
})

const User = mongoose.model("User", userSchema);

module.exports = User;